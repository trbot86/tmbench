#!/bin/bash

if [ "$#" -ne "1" ]; then
        echo "USAGE: $(basename $0) PNG_HREF_PREFIX"
        exit 1
fi

prefix=$1

./create_tex_png_listings.sh "$prefix"
for x in $(cat output_txt/*.txt | sort | uniq) ; do
        mkdir -p to_upload/$(dirname $x)
        cp $x to_upload/$x
done
cp output_tex/*.tex to_upload/
