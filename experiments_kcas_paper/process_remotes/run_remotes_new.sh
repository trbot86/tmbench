#!/bin/bash

# assumes all remotes share the same home directory
# copies the specified directory DIR into ~/run_remotes/{DIR}_{HOSTNAME}

if [ "$#" -eq "0" ]; then
    echo "USAGE: $(basename $0) TESTING_1_OR_0 DIRECTORY_TO_COPY REMOTE_HOST_NAME [REMOTE_HOST_NAME [...]]"
    exit 1
fi

testing=$1
shift
if [ "$testing" != "1" ] && [ "$testing" != "0" ]; then
    echo "must specify first argument TESTING_0_OR_1 properly"
    exit 1
fi

dir_to_copy=$(realpath $1)
echo "dir_to_copy=$dir_to_copy"
shift

shopt -s extglob                                        # enable +(...) glob syntax
dir_to_copy_basename=${dir_to_copy%%+(/)}               # trim however many trailing slashes exist
dir_to_copy_basename=${dir_to_copy_basename##*/}        # remove everything before the last / that still remains
echo "dir_to_copy_basename=$dir_to_copy_basename"

date
mkdir ~/run_remotes >/dev/null 2>&1
for host in $@ ; do
    echo "running: rm -rf ~/run_remotes/${dir_to_copy_basename}_$host >/dev/null 2>&1"
    rm -rf ~/run_remotes/${dir_to_copy_basename}_$host >/dev/null 2>&1
    rsync -av --progress --exclude 'bin' --exclude '*/bin' --exclude '*/.git' --exclude '.git' --exclude '*/_bak' --exclude '.*' --exclude '*.zip' --exclude '*.txt' --exclude '*.png' --exclude '_private' --exclude '_images' --exclude '*/data*_tm' --exclude '*/process_remotes/*_tm' --exclude '*/docker' --exclude '*.csv' --exclude '*.xls*' --exclude '*.db*' --exclude '*.sql*' --exclude '*/__pycache*' --exclude '*unused*' --exclude '*.ipynb' $dir_to_copy/ ~/run_remotes/${dir_to_copy_basename}_$host
done

dir=$(pwd)
reldir="${dir#"$dir_to_copy"}"
echo "dir=$dir"
echo "reldir=$reldir"

date
for host in $@ ; do
    remotedir="~/run_remotes/${dir_to_copy_basename}_${host}$reldir"
    echo "remotedir=$remotedir"
    if [ "$testing" == "1" ] ; then
        testing_str='testing'
    else
        testing_str=''
    fi
    echo "## cmd to run on ${host}: ssh -tt $host \"bash -c 'cd ${remotedir}/.. ; pwd ; ls ; ./run.sh $testing_str'\" &"
    ssh -tt $host "bash -c 'cd ${remotedir}/.. ; pwd ; ls ; ./run.sh $testing_str'" &
done

date
echo "## waiting for remote jobs to end"
wait
date

./process_remotes_new.sh "$testing" "$dir_to_copy" $@
