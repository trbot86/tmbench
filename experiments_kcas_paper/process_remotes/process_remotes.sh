#!/bin/bash

if [ "$#" -eq "0" ]; then
    echo "USAGE: $(basename $0) TESTING_0_OR_1 REMOTE_HOST_NAME [REMOTE_HOST_NAME [...]]"
    exit 1
fi

testing=$1
shift
if [ "$testing" != "1" ] && [ "$testing" != "0" ]; then
    echo "must specify first argument TESTING_0_OR_1 properly"
    exit 1
fi

date
dir=$(pwd)
for host in $@ ; do
    cmd="bash -c ' cd ${dir}/.."
    if [ "$testing" == "1" ]; then
        cmd="$cmd ; ../setbench/tools/data_framework/run_experiment.py _exp_non_tm.py -dpw --continue-on-warn-agg-row-count --testing"
        cmd="$cmd ; ../setbench/tools/data_framework/run_experiment.py _exp_tm.py -dpw --continue-on-warn-agg-row-count --testing"
    else
        cmd="$cmd ; ../setbench/tools/data_framework/run_experiment.py _exp_non_tm.py -dpw --continue-on-warn-agg-row-count"
        cmd="$cmd ; ../setbench/tools/data_framework/run_experiment.py _exp_tm.py -dpw --continue-on-warn-agg-row-count"
    fi
    cmd="$cmd '"

    echo "## cmd to run on ${host}: ${cmd}"
    ssh -tt $host "$cmd" > log_cmd_${host}.txt &
done
echo "## waiting for commands to run on remote hosts"
wait

for host in $@ ; do
    date
    echo "## rsyncing host ${host}:${dir}/../data* to local:."
    rsync -rp ${host}:${dir}/../data* .
    scp ${host}:${dir}/../output_log.txt ./output_log_${host}.txt
    if [ "$?" -ne "0" ]; then
        echo "error rsyncing for host ${host}"
        exit 1
    fi

    echo "## renaming directories from ${host}"
    for d in $(ls -d data*) ; do
        newname=${d//data_/${host}_}
        rm -r $newname 2>/dev/null
        mv $d $newname
        if [ "$?" -ne "0" ]; then
            echo "error doing mv oldname=$d newname=$newname"
            exit 1
        fi
    done
    echo "## done renaming"
done

date
echo "## adding sidenav headers"
../../setbench/tools/data_framework/add_sidenav_header.sh . > log_sidenav.txt

date
echo "## zipping for deployment"
zip -0rq to_deploy.zip *

echo "## copying to server"
scp to_deploy.zip linux.student.cs.uwaterloo.ca:public_html/kcas_paper/

date
echo "## deploying website"
ssh linux.student.cs.uwaterloo.ca 'bash -c "cd public_html/kcas_paper/ ; unzip -o to_deploy.zip"'

date
echo "## finished"

## clean up
rm to_deploy.zip 2>/dev/null
