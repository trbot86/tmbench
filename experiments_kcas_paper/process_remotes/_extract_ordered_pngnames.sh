#!/bin/bash

if [ "$#" -ne "1" ]; then
	echo "USAGE: $(basename $0) INPUT_HTML_FILE"
	exit 1
fi

f=$1
#cat $f | grep ".png" | sed 's/.*<td>.*<img src="//g' | sed 's/" border.*t[rd]>//g' | sed 's/<img src="//g' | sed 's/" width.*//g'

path=${f//\.\//}
path=${path%\/*}
#echo $path
cat $f | grep ".png" | sed 's/.*<td>.*<img src="//g' | sed 's/" border.*//g' | sed 's/<img src="//g' | sed 's/" width.*//g' | sed "s/\(.*\)/${path}\/\1/g"
