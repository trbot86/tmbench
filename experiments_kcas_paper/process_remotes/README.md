These scripts are intended to be run on rift. The scripts assume all remote servers share the same home directory, but this could be fixed by tweaking the rsync commands...

The directory you run in doesn't much matter. (It will be copied to a separate location for each remote host.)

To run and process results, run ./run_remotes_new.sh
Run with no args for instructions.

To fetch and results only, run ./process_remotes_new.sh
Run with no args for instructions.

process remotes is automatically invoked by run remotes.


A nominal command for these experiments is:
    ./run_remotes_new.sh 0 ../.. jax nasus

    (test first with 1 instead of 0)

If you want to check for possible host key issues with automatic website deployment first, try:
    ssh linux.student.cs.uwaterloo.ca

You should also be able to ssh into the various remote hosts by doing:
    ssh {hostname}
