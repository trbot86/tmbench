#!/bin/bash

./_create_page_pnglists.sh
mkdir output_tex 2>/dev/null
for f in $(ls output_txt/) ; do
	name=${f%.*}
	cat output_txt/$f | \
		(while read png ; do \
			if [[ "$png" == *"legend"* ]]; then \
				echo "\\includegraphics[width=\\legendwidth]{$png}" ; \
			else \
				echo "\\includegraphics[width=\\plotwidth]{$png}" ; \
			fi ;
		done) > output_tex/${name}.tex ; done

echo "output files:"
ls output_tex
echo
echo "done: see output_tex/..."
