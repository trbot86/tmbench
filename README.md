tmbench is essentially a big wrapper around setbench, another project. setbench is a benchmark for performing timed experiments where n threads perform search, insert and delete operations on a dictionary/map that stores keys and values. it implements many different data structures, but they aren't generally compatible with transactional memory. in tmbench/ds_tm, some data structures are implemented that are compatible with transactional memory. these data structures are implemented in such a way that they can be integrated with setbench, which can then be used to perform synthetic experiments on them. the makefile for tmbench integrates these data structures, and the implementations of transactional memory in the tmlib folder, into setbench and builds binaries for every combination of data structure in ds_tm and every transactional memory implementation in the tmlib folder. so, if you want to run, say, an external bst (ds_tm/brown_ext_bst_tm_auto) with a simple hybrid TM algorithm hytm2 (tmlib/hytm2), you should be able to do so by running the binary bin/brown_ext_bst_tm_auto.debra.hytm2. this effectively runs the microbenchmark in setbench/microbench/main.cpp, which takes many different command line arguments. you can find them in the main function. here's a simple example:

first build:
make -j
 
then run:

cd bin
LD_PRELOAD=../setbench/lib/libjemalloc.so numactl -i all ./brown_ext_bst_tm_auto.debra.hytm2 -nwork 96 -nprefill 96 -i 5 -d 5 -rq 0 -k 200000 -nrq 0 -t 3000 -pin 0-23,96-119,24-47,120-143,48-71,144-167,72-95,168-191
 
the LD_PRELOAD bit uses jemalloc as the allocator instead of the slower default system allocator.
the numactl -i all bit makes results faster and more stable on large scale systems.
the -nwork parameter says how many threads should access the tree simultaneously.
the -nprefill parameter should match -nwork
the -i parameter says that percentage of operations should be insert (similarly -d is about delete operations)
-rq should be 0
-k defined how many possible keys can be operated on. if the percentages of inserts and deletes are the same (which they should be typically), then the tree should typically contain half of -k.
-t specifies how many milliseconds of measured experiment time (which begins after a prefilling warmup phase) should occur
-pin specifies a "thread pinning" or "thread binding" or "processor binding" policy. the magic numbers above work for our server "jax". you can find these numbers if you run "lscpu." if you can see where they come from, then you can adapt them as appropriate for your servers.
 
running this command should produce a bunch of output on the screen. look for "total throughput" and that will tell you how many operations (search/insert/delete) were performed in total per second. there are also many other types of debugging output produced. feel free to ask questions.

scripts exist in experiments_nonspec_paper (see run.sh and exp_tm.py) to run a huge suite of experiments and produce graphs (relating to the paper https://arxiv.org/abs/1907.02669). the scripts are quite sophisticated but you should hopefully be able to infer how they work by looking at the output graphs they produce. you won't have to use these scripts necessarily unless you want to produce graphs.

incidentally, the data structures in ds_tm use type annotation (special types for the fields of data structure nodes) to perform operator overloading. this operator overloading turns any reads and writes to the fields of data structure nodes into function calls. these operator overloading function calls are implemented in tmlib/tm.h, which imports a file called stm.h for the particular TM you want to use (located at tmlib/{some_tm}/stm.h), and calls the appropriate STM_ macros defined in that stm.h file to perform transactional loads and stores. the macros in stm.h call the appropriate tm functions for the specific tm, such as TxLoad and TxStore. these functions are declared in tmlib/{some_tm}/{some_tm}.h, and defined in tmlib/{some_tm}/{some_tm}.cpp. this architecture (especially the stm.h part) is a bit convoluted... the STM_ macros and stm.h files are a holdover from the TM implementations I started with long ago, which came from the STAMP transactional memory benchmarks.
